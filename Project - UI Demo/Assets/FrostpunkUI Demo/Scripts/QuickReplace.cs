using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class QuickReplace : MonoBehaviour {

    [SerializeField] GameObject prefab;

    [ContextMenu("Replace with Prefab")]
    void Replace () {
        GameObject _prefab = PrefabUtility.InstantiatePrefab(prefab, transform.parent) as GameObject;
        _prefab.transform.localScale = transform.localScale;
        _prefab.transform.position = transform.position;
        _prefab.transform.rotation = transform.rotation;
    }

}