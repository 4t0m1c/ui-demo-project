using System.Collections;
using System.Collections.Generic;
using GameActions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIPauseMenu2 : MonoBehaviour {
    UIMainMenu uiMainMenu;
    [SerializeField] Canvas pauseMenuCanvas;

    void Awake () {
        uiMainMenu = new UIMainMenu ();
        uiMainMenu.Enable ();
    }

    void Update () {
        if (uiMainMenu.UI.Pause.triggered) {
            pauseMenuCanvas.enabled = !pauseMenuCanvas.enabled;
        }
    }

    public void ResumeGame () {
        pauseMenuCanvas.enabled = false;
    }
    
}