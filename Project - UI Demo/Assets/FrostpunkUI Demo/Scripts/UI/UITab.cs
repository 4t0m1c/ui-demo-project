using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UITab : MonoBehaviour {

    public static UnityEvent OnDeselectTab = new UnityEvent ();

    [SerializeField] Color selectedColour, deselectedColour;
    [SerializeField] Image buttonImage;

    void OnEnable () {
        OnDeselectTab.AddListener (DeselectTab);
    }

    void DeselectTab () {
        buttonImage.color = deselectedColour;
    }

    public void SelectTab (Transform tab) {
        OnDeselectTab.Invoke ();
        buttonImage.color = selectedColour;
        tab.SetAsLastSibling ();
    }

}