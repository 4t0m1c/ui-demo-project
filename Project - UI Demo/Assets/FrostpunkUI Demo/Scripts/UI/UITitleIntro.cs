using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITitleIntro : MonoBehaviour {

    [SerializeField] Animator animator;
    [SerializeField] string triggerName;

    public void TriggerAnimator () {
        animator.SetTrigger (triggerName);
    }

}