using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UILoadMainMenu : MonoBehaviour {

    [SerializeField] int sceneIndex = 0;

    public void LoadMainMenu () {
        SceneManager.LoadSceneAsync (sceneIndex);
    }
}