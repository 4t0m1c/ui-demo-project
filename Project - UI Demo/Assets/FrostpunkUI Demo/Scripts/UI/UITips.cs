using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITips : MonoBehaviour {

    [SerializeField] List<string> tips = new List<string> ();
    [SerializeField] float letterSpeed = 0.1f;
    [SerializeField] Text tipText;

    void Awake () {
        StartCoroutine (CycleTips ());
    }

    IEnumerator CycleTips () {
        int currentIndex = 0;
        string currentTip = tips[currentIndex];

        while (true) {
            float seconds = currentTip.Length * letterSpeed;
            tipText.text = currentTip;

            yield return new WaitForSeconds (seconds);

            currentIndex ++;
            if (currentIndex > tips.Count-1){
                currentIndex = 0;
            }

            currentTip = tips[currentIndex];
        }
    }

}