using System.Collections;
using System.Collections.Generic;
using GameActions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UILoadScene : MonoBehaviour {
    [SerializeField] Text loadingProgressText;
    [SerializeField] Image loadingFill;
    [SerializeField] string sceneName;
    [SerializeField] GameObject completeText;
    [SerializeField] Animator animator;

    UIMainMenu uiMainMenu;
    void Awake () {
        uiMainMenu = new UIMainMenu ();
        uiMainMenu.Enable ();
    }

    void Start () {
        //Call the LoadButton() function when the user clicks this Button
    }

    public void LoadScene () {
        animator.SetTrigger ("LoadScene");
        //Start loading the Scene asynchronously and output the progress bar
        StartCoroutine (LoadingScene ());
    }

    IEnumerator LoadingScene () {
        yield return null;

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync (sceneName);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        Debug.Log ("Pro :" + asyncOperation.progress);
        //When the load is still in progress, output the Text and progress bar
        loadingProgressText.text = "0%";
        yield return new WaitForSeconds (1);

        while (!asyncOperation.isDone) {
            //Output the current progress

            loadingProgressText.text = (asyncOperation.progress * 100) + 10 + "%";
            loadingFill.fillAmount = asyncOperation.progress + 0.1f;

            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f) {
                //Change the Text to show the Scene is ready
                completeText.SetActive (true);
                //Wait to you press the space key to activate the Scene
                if (uiMainMenu.UI.AnyKey.triggered) {
                    //Activate the Scene
                    asyncOperation.allowSceneActivation = true;
                }
            }

            yield return null;
        }
    }
}