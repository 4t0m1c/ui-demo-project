using System.Collections;
using GameActions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UILoadScene2 : MonoBehaviour {
    [SerializeField] Text progressText;
    [SerializeField] Text continueText;
    [SerializeField] Image progressBar;
    [SerializeField] string sceneName;
    [SerializeField] Animator sceneLoadAnimator;

    UIMainMenu uiMainMenu;

    void Awake () {
        uiMainMenu = new UIMainMenu ();
        uiMainMenu.Enable ();
    }

    public void LoadButton () {
        sceneLoadAnimator.SetTrigger ("FadeIn");
        //Start loading the Scene asynchronously and output the progress bar
        StartCoroutine (LoadScene ());
    }

    IEnumerator LoadScene () {
        yield return null;
        progressText.text = 0 + "%";
        yield return new WaitForSeconds (1);

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync (sceneName);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        Debug.Log ("Pro :" + asyncOperation.progress);
        //When the load is still in progress, output the Text and progress bar

        while (!asyncOperation.isDone) {
            //Output the current progress
            progressText.text = (asyncOperation.progress * 100) + 10 + "%";
            progressBar.fillAmount = asyncOperation.progress + 0.1f;

            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f) {
                //Change the Text to show the Scene is ready
                continueText.gameObject.SetActive (true);
                //Wait to you press the space key to activate the Scene
                if (uiMainMenu.UI.AnyKey.triggered)
                    //Activate the Scene
                    asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }
}