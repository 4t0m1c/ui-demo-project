using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILoadingTips : MonoBehaviour {

    [SerializeField] Text tipsText;
    [SerializeField] List<string> tips = new List<string> ();
    [SerializeField] float timePerCharacter = 0.1f;

    int currentTipIndex = 0;

    void Start () {
        StartCoroutine (CycleTips ());
    }

    IEnumerator CycleTips () {
        string currentTip = tips[currentTipIndex];

        while (true) {
            float waitForText = timePerCharacter * currentTip.Length;
            tipsText.text = currentTip;
            yield return new WaitForSeconds (waitForText);

            currentTipIndex++;
            if (currentTipIndex > tips.Count - 1) {
                currentTipIndex = 0;
            }

            currentTip = tips[currentTipIndex];
        }
    }

}