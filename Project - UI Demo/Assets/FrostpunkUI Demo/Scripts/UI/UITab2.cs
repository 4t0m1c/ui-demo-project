using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UITab2 : MonoBehaviour {

    public static UnityEvent OnTabSelected = new UnityEvent ();

    [SerializeField] Image tabBG;
    [SerializeField] Color selectedColour, unselectedColour;

    void OnEnable () {
        OnTabSelected.AddListener (UnselectTab);
    }

    void UnselectTab () {
        tabBG.color = unselectedColour;
    }

    public void SelectTab (Transform transform) {
        OnTabSelected.Invoke ();

        transform.SetAsLastSibling ();

        tabBG.color = selectedColour;
    }

}