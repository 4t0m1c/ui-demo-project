using System.Collections;
using System.Collections.Generic;
using GameActions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIAnyKey : MonoBehaviour {

    UIMainMenu uiMainMenu;
    [SerializeField] Animator introAnimator;
    [SerializeField] UILoadMainMenu loadMainMenu;

    void Awake () {
        uiMainMenu = new UIMainMenu ();
        uiMainMenu.Enable ();
    }

    void Update () {
        if (uiMainMenu.UI.AnyKey.triggered) {
            introAnimator.SetTrigger ("FadeOut");
            loadMainMenu.LoadMainMenu ();
        }
    }

}