using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour {

    PlayerControls playerControls;
    Vector2 moveVector;
    [SerializeField] float moveSpeed = 5;

    void Start () {
        playerControls = new PlayerControls ();
        playerControls.Enable ();

        playerControls.Movement.Move.performed += MoveInput;
    }

    void MoveInput (InputAction.CallbackContext ctx) {
        moveVector = ctx.ReadValue<Vector2> ();
    }

    void Update () {
        Vector3 targetPosition = transform.position + new Vector3 (moveVector.x, 0, moveVector.y);
        transform.position = Vector3.Lerp (transform.position, targetPosition, Time.deltaTime * moveSpeed);
    }

}